import React from 'react';
import ReactDOM from 'react-dom';

import TodoList from './components/todo-list';

ReactDOM.render(<TodoList />,
    document.getElementById('root'));

