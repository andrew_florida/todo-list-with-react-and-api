import React, { Component } from 'react';

import './list-item-render.css'

export default class ListItemRender extends Component {
    state = {
        clazz: `list__item`,
    };

    onEdit = () => {
        this.state.clazz === `list__item` ? this.setState({clazz: `list__item list__item--edit`}) : 
            this.setState({ clazz: `list__item` })
    };

    render(){
        return (
            <li id={this.props.id} key={this.props.id} className={`${this.state.clazz}`} >
                <span className="list__content">{this.props.title}</span>
                <input onChange={(e) => this.props.onChange(e)} type="text" value={this.props.title} className="list__input"></input>
                <button onClick={this.onEdit}>Edit</button>
                <button onClick={this.props.onDelete}>Delete</button>
            </li>
        )
    }
}

