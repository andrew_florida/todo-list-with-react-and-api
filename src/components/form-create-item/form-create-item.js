import React, { Component } from 'react';

import './form-create-item.css';

export default class FormCreateItem extends Component {

    state = {
        title: ''
    };

    onLabelChange = (e) => {
        this.setState({
            title: e.target.value
        })
    };

    onSubmit = (e) => {
        e.preventDefault();
        const { title } = this.state;
        this.setState({ title: '' });
        const cb = this.props.onCreateItem || (() => { });
        cb(title);
    };

    render() {
        return (
            <form
                onSubmit={this.onSubmit}>

                <input type="text"
                    value={this.state.title}
                    onChange={this.onLabelChange}
                    placeholder="Create task" />

                <button type="submit">Add</button>
            </form>
        );
    }
}
