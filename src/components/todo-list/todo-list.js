import React, { Component } from 'react';

import FormCreateItem from '../form-create-item';
import ListItemRender from '../list-item-render'

const url = `https://jsonplaceholder.typicode.com/todos/`;

export default class TodoList extends Component {
    maxId = 200;

    state = {
        obj: [],
    };

    async componentDidMount() {
        const response = await fetch(`${url}`);
        if (!response.ok) {
            throw new Error(`Could not fetch ${url} received ${response.status}`);
        }
        const data = await response.json();

        this.setState({ obj: data });
    }

    onDelete = (id) => {
        this.setState((state) => {
            const idx = state.obj.findIndex((item) => item.id === id);
            
            const obj = [
                ...state.obj.slice(0, idx),
                ...state.obj.slice(idx + 1)
            ];
            return { obj };
        });
    };

    onChange = (id, title, e) => {

        let val = e.target.value;
        
        this.setState((state) => {
            const idx = state.obj.findIndex((item) => item.id === id);
            
            const obj = [ ...state.obj ];
            obj[idx]['title'] = val; 
            return { obj };
        });
    };

    onCreateItem = (title) => {
        this.setState((state) => {
            const item = this.createItem(title);
            return { obj: [...state.obj, item] };
        })
    };

    createItem(title) {
        return {
            id: ++this.maxId,
            title,
        };
    }
    
    render() {
        const { obj } = this.state;
        return (
            <div className="todos" >
                <FormCreateItem onCreateItem={this.onCreateItem}/>
                <ul>
                    {obj.map(el =>
                        <ListItemRender id={el.id} title={el.title} key={el.id} onChange={(e) => this.onChange(el.id, el.title, e)} onDelete={() => this.onDelete(el.id)} />
                    )}
                </ul>
            </div>
        );
    }

}


